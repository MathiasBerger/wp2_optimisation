from pyomo.environ import ConcreteModel, Param, RangeSet, Var, Constraint, Objective, Binary, minimize
from pyomo.opt import SolverFactory, ProblemFormat
from numpy import random

def model_builder(n_L, n_W, D, n, eps, beta, m, write_lp=False):

    model = ConcreteModel()

    model.n_L = Param(initialize=n_L)
    model.n_W = Param(initialize=n_W)

    model.L = RangeSet(1, model.n_L)
    model.W = RangeSet(1, model.n_W)

    model.D = Param(model.L, model.W, mutable=True)
    for i_l in range(n_L):
        for i_c in range(n_W):
            model.D[i_l+1, i_c+1] = D[i_l, i_c]
    model.n = Param(initialize=n)
    model.eps = Param(initialize=eps)
    model.beta = Param(initialize=beta)
    model.m = Param(initialize=m)

    model.a = Var(model.L, within=Binary)
    model.b = Var(model.W, within=Binary)

    def cardinality_constraint_rule(model):
        return sum(model.a[l] for l in model.L) >= model.n

    def criticality_activation_constraint_rule(model, w):
        return model.beta - (1 / model.n) * sum(model.D[l, w] * model.a[l] for l in model.L) >= model.eps + (model.m - model.eps) * model.b[w]

    model.cardinality_constraint = Constraint(rule=cardinality_constraint_rule)
    model.criticality_activation_constraint = Constraint(model.W, rule=criticality_activation_constraint_rule)

    def cost_rule(model):
        return sum(model.b[w] for w in model.W)

    model.cost = Objective(rule=cost_rule, sense=minimize)
    
    if write_lp:
        model.write(filename="model.lp",
            format=ProblemFormat.cpxlp,
            io_options={"symbolic_solver_labels": True})
        model.write(filename='model.mps',
            format=ProblemFormat.mps)

    return model


if __name__ == "__main__":
    
    n_L = 100
    n_W = 1000
    D = random.rand(n_L, n_W)
    for i_l in range(D.shape[0]):
        for i_c in range(D.shape[1]):
            if D[i_l, i_c] > 0.5:
                D[i_l, i_c] = 1.
            else:
                D[i_l, i_c] = 0.
    n = 5
    eps = 1e-3
    beta = 1
    m = beta - n_W / n

    model = model_builder(n_L, n_W, D, n, eps, beta, m, write_lp=False)

    opt = SolverFactory("gurobi", solver_io='python')
#    opt.options['MIPGap'] = 0.05
    opt.options['Method'] = 2
    opt.options['OutputFlag'] = 1
    opt.options['LogFile'] = 'gurobi.log'

    results = opt.solve(model, tee=True, keepfiles=False)    



